const getLatLngFromGoogleResult = result => {
  const location = result.geometry.location;
  return `${location.lat},${location.lng}`;
}

export default getLatLngFromGoogleResult;
