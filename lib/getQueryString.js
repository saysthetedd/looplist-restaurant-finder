import store    from 'app/store';
import google   from 'api/google';
import isLatLng from './isLatLng';

const getQueryString = () => {
  const {
    query,
    radius,
    openNow,
    price,
  } = store.getState().search;

  const buildQueryString = (location, radius, openNow, price) => {
    const queryString = 'location='
                        + location
                        + '&radius='
                        + radius
                        + '&opennow='
                        + openNow
                        + '&maxprice='
                        + price
                        + '&type=restaurant';
    return queryString;
  }

  return new Promise((resolve, reject) => {
    if (!isLatLng(query)) {
      google.getLatLng(query)
      .then(result => {
        resolve(buildQueryString(result, radius, openNow, price));
      })
    } else {
      resolve(buildQueryString(query, radius, openNow, price));
    }
  })
}

export default getQueryString;
