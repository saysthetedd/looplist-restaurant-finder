module.exports  = {
  flexCenter: {
    'display'         : 'flex',
    'align-items'     : 'center',
    'justify-content' : 'center'
  },

  fadeIn: {
    'opacity'    : '1',
    'visibility' : 'visible',
    'transition' : 'all ease-out 0.3s'
  },

  fadeOut: {
    'opacity'    : '0',
    'visibility' : 'hidden',
    'transition' : 'all ease-in 0.3s'
  },

  fadeUpInHide: {
    'opacity'    : '0',
    'visibility' : 'hidden',
    'transform'  : 'translateY(100%)',
    'transition' : 'all ease-in 0.3s'
  },

  fadeUpInShow: {
    'opacity'    : '1',
    'visibility' : 'visible',
    'transform'  : 'translateY(0%)',
    'transition' : 'all ease-out 0.3s'
  },

  scaleInShow: {
    'opacity'    : '1',
    'visibility' : 'visible',
    'transform'  : 'translateY(0%) scale(1)',
    'transition' : 'all ease-in 0.2s'
  },

  scaleInHide: {
    'opacity'    : '0',
    'visibility' : 'hidden',
    'transform'  : 'translateY(10%) scale(0)',
    'transition' : 'all ease-in 0.2s'
  },
}
