import blue  from './blue';
import dark  from './dark';
import light from './light';

export default {
  blue,
  dark,
  light,
}
