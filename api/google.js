import appInfo                    from 'constants/appInfo';
import getLatLngFromGoogleResult  from 'lib/getLatLngFromGoogleResult';
import fetchJsonp   from 'fetch-jsonp';

const postSearch = query => {
  const url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
              + query
              + '&key='
              + appInfo.googlePlacesAPIKey;
  return fetch(url)
           .then(response => response.json())
           .then(responseAsJson => responseAsJson.results);
}

const getLatLng = address => {
  const url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
              + address
              + '&key='
              + appInfo.googleGeocodingAPIKey;

  return fetch(url)
           .then(response => response.json())
           .then(responseAsJson => responseAsJson.results)
           .then(results => getLatLngFromGoogleResult(results[0]));
}

const getPhoto = reference => {
  const url = 'https://maps.googleapis.com/maps/api/place/photo?'
              + 'maxwidth=900'
              + 'photoreference='
              + reference
              + '&key='
              + appInfo.googlePlacesAPIKey;
}

export default {
  postSearch,
  getLatLng,
};
