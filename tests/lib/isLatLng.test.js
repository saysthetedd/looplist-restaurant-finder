import isLatLng from '../../lib/isLatLng';

test('Checks if a string is a valid set of latitude + longitude coordinates', () => {
  const validLatLng = '47.1231231, 179.99999999';
  const invalidLatLng = '-91, 123.456';
  expect(isLatLng(validLatLng)).toBe(true);
  expect(isLatLng(invalidLatLng)).toBe(false);
});
