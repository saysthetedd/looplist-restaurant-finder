import getLatLngFromGoogleResult from '../../lib/getLatLngFromGoogleResult';

test('returns a comma seperated lat and lng from a google geocode api result', () => {
  const result = {
    name: "Denver",
    geometry: {
      location: {
        "lat": 39.7392358,
        "lng": -104.990251,
      }
    }
  }
  const validLatLng = '39.7392358,-104.990251';
  
  expect(getLatLngFromGoogleResult(result)).toBe(validLatLng);
});
