import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';

import appActionCreators      from '../actionCreators/app';
import mapActionCreators      from '../actionCreators/map';
import searchActionCreators   from '../actionCreators/search';
import RestaurantFinder       from '../components/RestaurantFinder';

const mapStateToProps = state => {
  return {
    app    : state.app,
    map    : state.map,
    search : state.search,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    appActionCreators    : bindActionCreators(appActionCreators, dispatch),
    mapActionCreators    : bindActionCreators(mapActionCreators, dispatch),
    searchActionCreators : bindActionCreators(searchActionCreators, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RestaurantFinder);
