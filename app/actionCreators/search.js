import google         from 'api/google';
import getQueryString from 'lib/getQueryString';
import app            from './app';
import map            from './map';


const setSearchQuery = query => {
  return {
    type: 'SET_SEARCH_QUERY',
    value: query
  }
}

const setRadius = radius => {
  return {
    type: 'SET_RADIUS',
    value: radius
  }
}

const setPrice = price => {
  return {
    type: 'SET_PRICE',
    value: price
  }
}

const setOpenNow = open => {
  return {
    type: 'SET_OPEN_NOW',
    value: open
  }
}

const setIsActive = isActive => {
  return {
    type: 'SET_SEARCH_IS_ACTIVE',
    value: isActive
  }
}

const searchGooglePlaces = () => {
  return dispatch => {
    dispatch(app.setIsSearching(true));
    dispatch(
      getQueryString()
      .then(query => {
        google.postSearch(query)
        .then(results => {
          dispatch(map.setDrawerIsActive(true));
          dispatch(setSearchResults(results));
          dispatch(app.setIsSearching(false));
        })
      })
    );
  }
}

const setSearchResults = results => {
  return dispatch => dispatch({ type: 'SET_RESULTS', value: results });
}

const getCurrentLocation = () => {
  return dispatch => {
    if ("geolocation" in navigator) {
      dispatch(app.setIsSearching(true));

      navigator.geolocation.getCurrentPosition(position => {
        dispatch(setSearchQuery(`${position.coords.latitude}, ${position.coords.longitude}`));
        dispatch(app.setIsSearching(false));
        dispatch(map.setCenter(position.coords.latitude, position.coords.longitude));
        dispatch(searchGooglePlaces());
      });
    }
    else {
      alert('Your browser doesn\'t support geolocation');
    }
  }
}

export default {
  setSearchQuery,
  setRadius,
  setPrice,
  setOpenNow,
  searchGooglePlaces,
  setSearchResults,
  setIsActive,
  getCurrentLocation,
}
