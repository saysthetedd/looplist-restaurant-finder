const setIsSearching = isSearching => ({
  type: 'SET_IS_SEARCHING',
  value: isSearching,
});

const setIsGettingCurrentLocation = isGettingCurrentLocation => ({
  type: 'SET_IS_GETTING_CURRENT_LOCATION',
  value: isGettingCurrentLocation,
});

export default {
  setIsSearching,
  setIsGettingCurrentLocation,
}
