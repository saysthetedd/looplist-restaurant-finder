const setTheme = theme => {
  return {
    type: 'SET_THEME',
    value: theme
  }
}

const setDrawerIsActive = isActive => {
  return {
    type: 'SET_DRAWER_IS_ACTIVE',
    value: isActive
  }
}

const setCenter = (lat, lng) => {
  return {
    type: 'SET_MAP_CENTER',
    value: { lat, lng }
  }
}

export default {
  setTheme,
  setDrawerIsActive,
  setCenter,
}
