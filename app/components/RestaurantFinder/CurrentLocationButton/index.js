import React, { Component } from 'react';
import styles               from './styles.css';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class CurrentLocationButton extends Component {
  handleClick() {
    this.props.searchActions.getCurrentLocation();
  }

  getRootStyles() {
    const isActive = this.props.isActive;
    return cx({
      Root: true,
      isActive
    })
  }

  getImage() {
    if (this.props.isActive)            return 'https://s3-us-west-2.amazonaws.com/looplist-restaurant/rolling.gif';
    if (this.props.search.query.length) return 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/VisualEditor_-_Icon_-_Search-big_-_white.svg/2000px-VisualEditor_-_Icon_-_Search-big_-_white.svg.png';
    return 'https://s3-us-west-2.amazonaws.com/looplist-restaurant/current-location.svg'
  }

  getHelperText() {
    if (this.props.search.query.length)  return null;
    if (this.props.isActive) return null;
    return <span>Search my current location</span>
  }

  render() {
    return (
      <a
        className={ this.getRootStyles() }
        onClick={ () => this.handleClick() }
      >
        <img src={ this.getImage() } />
        { this.getHelperText() }
      </a>
    );
  }
}

export default CurrentLocationButton;
