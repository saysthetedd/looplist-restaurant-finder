import React, { Component } from 'react';
import styles               from './styles.css';
import ReactStars           from 'react-stars';
import StarRatingComponent  from 'react-star-rating-component';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class Drawer extends Component {
  renderRadiusValue() {
    if (this._input)
    return this._input ? this._input.value : 'non';
  }

  getStyles() {
    return cx({
      Root: true,
      isActive: this.props.isActive
    })
  }

  updateRadius() {
    const radius = this._input.value;
    this.props.searchActions.setRadius(radius);
  }

  updateOpenNow() {
    const checked = this._openNow.checked;
    this.props.searchActions.setOpenNow(checked);
  }

  getCurrentMeters() {
    return this._input ? this._input.value : '';
  }

  updatePrice(price) {
    console.log(price);
    this.props.searchActions.setPrice(price);
  }

  render() {
    return (
      <div className={ this.getStyles() }>
        <section>
          <span>Radius <b>{ this.getCurrentMeters() } meters</b></span>
          <input
            min="0"
            max="50000"
            type="range"
            step="1000"
            onMouseUp={ () => this.updateRadius() }
            ref={(c) => this._input = c}
            defaultValue={ this.props.search.radius }
          />
          <ul className={ styles.RadiusEnds }>
            <li>0</li>
            <li>50000</li>
          </ul>
        </section>

        <section
          className={ styles.Price }
        >
          <span>Price Range</span>
          <StarRatingComponent
              name="price"
              starCount={4}
              starColor={'#1BAAE4'}
              emptyStarColor={'#bbb'}
              value={this.props.search.price}
              onStarClick={(price) => this.updatePrice(price)}
          />
        </section>

        <section className={ styles.OpenNow }>
          <span>Open Now</span>
          <input
            ref={(c) => this._openNow = c}
            onChange={() => this.updateOpenNow() }
            type="checkbox"
            defaultValue={ this.props.search.openNow }
          />
        </section>
          <button
            className={ styles.SendButton }
            disabled={!this.props.search.query.length }
          >
            Search
          </button>
      </div>
    );
  }
}

export default Drawer;
