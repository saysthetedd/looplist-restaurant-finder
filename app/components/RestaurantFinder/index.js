import React            from 'react';
import Header           from './Header';
import Map              from './Map';
import MapDrawer        from './MapDrawer';
import SearchingOverlay from './SearchingOverlay';

const RestaurantFinder = props => (
  <div>
    <SearchingOverlay
      app={ props.app }
      search={ props.search }
    />
    <Header
      app={ props.app }
      appActions={ props.appActionCreators }
      search={ props.search }
      searchActions={ props.searchActionCreators }
    />
    <MapDrawer
      app={ props.app }
      appActions={ props.appActionCreators }
      map={ props.map }
      mapActions={ props.mapActionCreators }
      search={ props.search }
      searchActions={ props.searchActionCreators }
    />
    <Map
      app={ props.app }
      appActions={ props.appActionCreators }
      map={ props.map }
      mapActions={ props.mapActionCreators }
      search={ props.search }
      searchActions={ props.searchActionCreators }
    />
  </div>
);

export default RestaurantFinder;
