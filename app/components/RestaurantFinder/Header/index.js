import React       from 'react';
import styles      from './styles.css';
import SearchInput from '../SearchInput';

const Header = props => (
  <header
    id="app-header"
    className={ styles.Root }
  >
    <img
      className={ styles.Logo }
      src="http://static1.squarespace.com/static/546037b1e4b01809e286d9ee/t/57a90128e6f2e1756d4d4f1f/1471635731614/?format=1500w"
    />
    <SearchInput
      app={ props.app }
      appActions={ props.appActions }
      search={ props.search }
      searchActions={ props.searchActions }
    />
  </header>
);

export default Header;
