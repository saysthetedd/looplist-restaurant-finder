import React     from 'react';
import styles    from './styles.css';
import Draggable from 'react-draggable';

const Controls = props => (
  <Draggable>
    <div className={ styles.Root }>
      <button onClick={() => props.zoomIn() }>+</button>
      <button onClick={() => props.zoomOut() }>-</button>
    </div>
  </Draggable>
);

export default Controls;
