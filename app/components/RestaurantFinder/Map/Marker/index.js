import React, { Component } from 'react';
import styles               from './styles.css';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class Marker extends Component {
  constructor() {
    super();

    this.state = {
      isActive: false,
    }
  }

  handleClick() {
    this.props.mapActions.setCenter(this.props.lat, this.props.lng);
    this.toggleActive();
  }

  toggleActive() {
    const isActive = !this.state.isActive;
    this.setState({ isActive });
  }

  getRootStyles() {
    const isActive = this.state.isActive;
    return cx({
      Root: true,
      isActive
    })
  }

  render() {
    return (
      <div
        className={ this.getRootStyles() }
        onClick={ () => this.handleClick() }
      >
        <div className={ styles.Details }>
          <h1>{ this.props.name }</h1>
          <h3>{ this.props.open ? 'They Are Open' : 'Closed' }</h3>
        </div>
        <img src="http://static1.squarespace.com/static/546037b1e4b01809e286d9ee/t/57a90128e6f2e1756d4d4f1f/1471635731614/?format=1500w" />
        <div className={ styles.Spinner } />
      </div>
    );
  }
}

export default Marker;
