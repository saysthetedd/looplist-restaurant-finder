import React, { Component } from 'react';
import styles               from './styles.css';
import GoogleMap            from 'google-map-react';
import Marker               from './Marker';
import Controls             from './Controls';
import mapThemes            from '../../../../constants/mapThemes';
import appInfo              from 'constants/appInfo';

class Map extends Component {
  handleClick() {
    this.props.searchActions.setIsActive(false);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.map.center !== this.props.map.center) {
      this.updateCenter(newProps.map.center.lat, newProps.map.center.lng);
    }

    if (newProps.search.results !== this.props.search.results) {
      const result = newProps.search.results[0].geometry.location;
      this.updateCenter(result.lat, result.lng);
      this.map.setZoom(14);
    }
  }

  renderMarkers() {
    const results = this.props.search.results;
    if (results.length) {
      return results.map(result => (
        <Marker
          name={ result.name }
          lat={ result.geometry.location.lat }
          lng={ result.geometry.location.lng }
          open={ result.opening_hours.open_now }
          mapActions={ this.props.mapActions }
        />
      ))
    }
    return false;
  }

  updateCenter(lat, lng) {
    this.map.panTo({lat, lng});
  }

  setMap(map, maps) {
    this.map  = map;
    this.maps = maps;
  }

  zoomIn() {
    const zoom = this.map.getZoom() + 1;
    this.map.setZoom(zoom);
  }

  zoomOut() {
    const zoom = this.map.getZoom() - 1;
    this.map.setZoom(zoom);
  }

  render() {
    return (
      <div
        className={ styles.Root }
        onClick={() => this.handleClick() }
      >
        <Controls
          zoomIn={ this.zoomIn.bind(this) }
          zoomOut={ this.zoomOut.bind(this) }
        />
        <GoogleMap
          apiKey={appInfo.googleMapAPIKey}
          defaultZoom={1}
          defaultCenter={{
            lat: 39.74717,
            lng: -104.990307
          }}
          bootstrapURLKeys={{
            key: appInfo.googleMapAPIKey
          }}
          options={{
            styles: mapThemes[this.props.map.theme || 'blue'],
            center: {
              lat: this.props.map.center.lat,
              lng: this.props.map.center.lng
            },
            zoomControl: false,
          }}
          onGoogleApiLoaded={({map, maps}) => this.setMap(map, maps)}
          yesIWantToUseGoogleMapApiInternals
        >
          { this.renderMarkers() }
        </GoogleMap>
      </div>
    );
  }
}

export default Map;
