import React, { Component } from 'react';
import styles               from './styles.css';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class SearchingOverlay extends Component {
  getRootStyles() {
    const isActive = this.props.app.isSearching;
    return cx({
      Root: true,
      isActive,
    })
  }

  render() {
    return (
      <div className={this.getRootStyles()}>
        <div>
          <img src="https://s3-us-west-2.amazonaws.com/looplist-restaurant/rolling-blue.gif" alt=""/>
          <h1>Searching for</h1>
          <h3>{ this.props.search.query ? this.props.search.query : 'your location' }</h3>
          <h5>Within: { this.props.search.radius } meters</h5>
          <h5>{ this.props.search.openNow ? 'only open now' : '' }</h5>
        </div>
      </div>
    );
  }
}

export default SearchingOverlay;
