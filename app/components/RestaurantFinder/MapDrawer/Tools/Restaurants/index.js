import React, { Component } from 'react';
import styles               from './styles.css';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class Restaurants extends Component {
  toggleActive() {
    const isActive = !this.props.map.drawerIsActive;
    this.props.mapActions.setDrawerIsActive(isActive);
  }

  getRootStyles() {
    const isActive = this.props.map.drawerIsActive;
    return cx({
      Root: true,
      isActive
    })
  }

  render() {
    return (
      <div
        className={ this.getRootStyles() }
      >
        <button onClick={() => this.toggleActive() }>
          <img src="https://s3.amazonaws.com/arcuri.wannago/drawer.svg" />
          <span>
            {
              this.props.map.drawerIsActive
              ? 'Close Results'
              : 'Search Results'
            }
          </span>
        </button>
      </div>
    );
  }
}

export default Restaurants;
