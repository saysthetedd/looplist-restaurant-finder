import React       from 'react';
import MapThemes   from './MapThemes';
import Restaurants from './Restaurants';
import styles      from './styles.css';

const Tools = props => (
  <div className={ styles.Root }>
    <Restaurants
      map={ props.map }
      mapActions={ props.mapActions }
    />
    <MapThemes
      map={ props.map }
      mapActions={ props.mapActions }
    />
  </div>
);

export default Tools;
