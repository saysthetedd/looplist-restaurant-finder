import React, { Component } from 'react';
import styles               from './styles.css';
import classNames           from 'classnames/bind';
import mapThemes            from 'constants/mapThemes';

const cx = classNames.bind(styles);

class MapThemes extends Component {
  constructor() {
    super();

    this.state = {
      isActive: false,
    }
  }

  toggleActive() {
    const isActive = !this.state.isActive;
    this.setState({ isActive });
  }

  getRootStyles() {
    const isActive = this.state.isActive;
    return cx({
      Root: true,
      isActive
    })
  }

  updateMapTheme(theme) {
    this.props.mapActions.setTheme(theme);
  }

  render() {
    return (
      <div className={ this.getRootStyles() }>
        <button onClick={() => this.toggleActive() }>
          <img src="https://s3.amazonaws.com/arcuri.wannago/globeZoom.svg" alt=""/>
          <span>Map Theme</span>
        </button>
        <ul>
          {
            Object.keys(mapThemes).map( theme => {
              return (
                <li
                  key={ theme }
                  onClick={ () => this.updateMapTheme(theme) }
                  className={ theme === this.props.map.theme ? styles.isActive : '' }
                >
                  <img src={`https://s3-us-west-2.amazonaws.com/looplist-restaurant/map-${theme}.png`} />
                  <span>{theme}</span>
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
}

export default MapThemes;
