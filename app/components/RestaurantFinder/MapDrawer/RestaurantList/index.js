import React      from 'react';
import Restaurant from './Restaurant';
import styles     from './styles.css';

const RestaurantList = props => {
  const render = () => {
    const results = props.search.results;
    if (results.length) {
      return results.map(result => (
        <Restaurant
          name={ result.name }
          open={ result.opening_hours.open_now }
          rating={ result.rating }
          onClick={ () => { props.mapActions.setCenter(
            result.geometry.location.lat,
            result.geometry.location.lng
          )}}
        />
      ));
    }
    return (
      <div>
        <h3>Search above to return restaurant suggestions from anywhere in the world.</h3>
        <button
          onClick={ () => props.searchActions.getCurrentLocation() }
        >
          Search my current destination
        </button>
      </div>
    );
  }

  return (
    <div className={ styles.Root }>
     { render() }
    </div>
  )
}

export default RestaurantList;
