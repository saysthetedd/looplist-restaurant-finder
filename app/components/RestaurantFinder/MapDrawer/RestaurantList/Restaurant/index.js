import React, { Component } from 'react';
import StarRatingComponent  from 'react-star-rating-component';
import styles               from './styles.css';

class Restaurant extends Component {
  render() {
    return (
      <li
        className={ styles.Root }
        onClick={ this.props.onClick }
      >
        <h1>{this.props.name}</h1>
        <div className={ styles.Stars }>
          <StarRatingComponent
              name={this.props.name}
              starCount={5}
              starColor={'#1BAAE4'}
              emptyStarColor={'#bbb'}
              value={ this.props.rating }
              editing={false}
          />
        </div>
        <p>{this.props.open ? 'Open' : 'Closed'}</p>
      </li>
    );
  }
}

export default Restaurant;
