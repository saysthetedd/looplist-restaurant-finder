import React, {Component} from 'react';
import Tools              from './Tools';
import RestaurantList     from './RestaurantList';
import styles             from './styles.css';
import classNames           from 'classnames/bind';

const cx = classNames.bind(styles);

class MapDrawer extends Component {
  getRootStyles() {
    const isActive = this.props.map.drawerIsActive;
    return cx({
      Root: true,
      isActive
    })
  }

  render() {
    return (
      <div className={ this.getRootStyles() }>
        <Tools
          map={ this.props.map }
          mapActions={ this.props.mapActions }
        />
        <div className={ styles.Drawer }>
          <RestaurantList
            search={ this.props.search }
            searchActions={ this.props.searchActions }
            mapActions={ this.props.mapActions }
          />
        </div>
      </div>
    )
  }
}

export default MapDrawer;
