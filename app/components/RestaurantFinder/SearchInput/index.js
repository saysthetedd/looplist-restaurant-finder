import React, { Component }  from 'react';
import styles                from './styles.css';
import CurrentLocationButton from '../CurrentLocationButton';
import SearchDrawer          from '../SearchDrawer';

class SearchInput extends Component {
  setSearchIsActive (isActive) {
    this.props.searchActions.setIsActive(isActive);
  }

  handleChange() {
    const query = this.refs.search.value;
    this.props.searchActions.setSearchQuery(query);
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.props.search.query.length) {
      this.props.searchActions.searchGooglePlaces();
    }
  }

  render() {
    return (
      <form
        className={ styles.Root }
        onSubmit={(e) => this.handleSubmit(e) }
      >
        <CurrentLocationButton
          isActive={ this.props.app.isSearching }
          search={ this.props.search }
          searchActions={ this.props.searchActions }
        />
        <input
          ref="search"
          className={ styles.MainInput }
          onFocus={ () => this.setSearchIsActive (true)  }
          onChange={ () => this.handleChange() }
          placeholder="Search for something delicious"
          value={ this.props.search.query }
        />
        <SearchDrawer
          isActive={ this.props.search.isActive }
          search={ this.props.search }
          searchActions={ this.props.searchActions }
        />
        <button
          type="submit"
          hidden
        />
      </form>
    );
  }
}

export default SearchInput;
