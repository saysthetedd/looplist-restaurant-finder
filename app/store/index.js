import { applyMiddleware, createStore, compose } from 'redux';
import ReduxThunk                                from 'redux-thunk';
import promiseMiddleware                         from 'redux-promise';
import reducers                                  from '../reducers';

const middleware = [ReduxThunk, promiseMiddleware];
const store = createStore(reducers, {}, compose(
  applyMiddleware(...middleware),
  window.devToolsExtension ? window.devToolsExtension() : f => f
));

export default store;
