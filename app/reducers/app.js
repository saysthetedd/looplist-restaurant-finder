import app           from '../store/initialState/app';
import createReducer from '../../lib/createReducer';

export default createReducer(app, {
  SET_IS_SEARCHING(state, { value }) {
    return {
      ...state,
      isSearching: value,
    }
  },
  SET_IS_GETTING_CURRENT_LOCATION(state, { value }) {
    return {
      ...state,
      isGettingCurrentLocation: value,
    }
  },
});
