import search from '../store/initialState/search';
import createReducer from '../../lib/createReducer';

export default createReducer(search, {
  SET_SEARCH_IS_ACTIVE(state, { value }) {
    return {
      ...state,
      isActive: value,
    }
  },

  SET_SEARCH_QUERY(state, { value }) {
    return {
      ...state,
      query: value,
    }
  },

  SET_RADIUS(state, { value }) {
    return {
      ...state,
      radius: value,
    }
  },

  SET_PRICE(state, { value }) {
    return {
      ...state,
      price: value,
    }
  },

  SET_OPEN_NOW(state, { value }) {
    return {
      ...state,
      openNow: value,
    }
  },

  SET_RESULTS(state, { value }) {
    return {
      ...state,
      results: value,
    }
  },
});
