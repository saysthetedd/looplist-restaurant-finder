import { combineReducers } from 'redux';
import appReducer          from './app';
import mapReducer          from './map';
import searchReducer       from './search';

export default combineReducers({
  app    : appReducer,
  map    : mapReducer,
  search : searchReducer,
});
