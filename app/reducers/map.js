import map from '../store/initialState/map';
import createReducer from '../../lib/createReducer';

export default createReducer(map, {
  SET_THEME(state, { value }) {
    return {
      ...state,
      theme: value,
    }
  },

  SET_DRAWER_IS_ACTIVE(state, { value }) {
    return {
      ...state,
      drawerIsActive: value,
    }
  },

  SET_MAP_CENTER(state, { value }) {
    return {
      ...state,
      center: value,
    }
  },
});
