var postCSSConfig = require('./config/postcss.config.js');
var path          = require('path');

module.exports = {
  entry: './app/index.js',
  output: {
    path: './build',
    filename: 'app.bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        loader: 'style!css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss'
      }
    ]
  },
  postcss: function() {
    return postCSSConfig;
  },
  resolve: {
    root: path.resolve('./'),
    extensions: ['', '.js']
  }
};
