var postCSSConfig = [
  require('autoprefixer'),
  require('postcss-media-minmax'),
  require('rucksack-css'),
  require('postcss-mixins')({
    mixins: require('../shared/styles/mixins')
  }),
  require('postcss-for'),
  require('postcss-nested'),
  require('postcss-simple-vars')({
    variables: function variables() {
      return require('../shared/styles/variables')
    },
    unknown: function unknown(node, name, result) {
      node.warn(result, 'Unknown variable ' + name)
    }
  }),
  require('postcss-math'),
  require('postcss-color-function'),
  require('postcss-sass-colors'),
];

module.exports = postCSSConfig;
